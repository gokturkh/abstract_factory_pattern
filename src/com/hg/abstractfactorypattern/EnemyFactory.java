package com.hg.abstractfactorypattern;

public final class EnemyFactory {

    public static Enemy makeEnemy(EnemyFactoryInterface factory) {
        return factory.createEnemy();
    }
}
