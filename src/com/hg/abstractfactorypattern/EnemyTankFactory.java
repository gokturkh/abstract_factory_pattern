package com.hg.abstractfactorypattern;

public class EnemyTankFactory implements EnemyFactoryInterface {
    private int power;
    private int damage;
    private String type;

    public EnemyTankFactory(String type, int power, int damage) {
        this.type = type;
        this.power = power;
        this.damage = damage;
    }

    @Override
    public Enemy createEnemy() {
        return new EnemyTank(this.type, this.power, this.damage);
    }
}
