package com.hg.abstractfactorypattern;

public class EnemyShip extends Enemy {
    public EnemyShip(String type, int power, int damage) {
        super(type, power, damage);
    }

    @Override
    protected void attack() {
        System.out.println("Ship attacked.");
    }

    @Override
    protected void getStats() {
        System.out.println("Enemy type is: " + this.getType());
        System.out.println("Enemy power is: " + this.getPower());
        System.out.println("Enemy damage is: " + this.getDamage());
    }
}
