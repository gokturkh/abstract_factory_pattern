package com.hg.abstractfactorypattern;

abstract public class Enemy {
    private int power;
    private int damage;
    private String type;

    public Enemy(String type, int power, int damage) {
        this.type = type;
        this.power = power;
        this.damage = damage;
    }

    public String getType() {
        return type;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    protected abstract void attack();

    protected abstract void getStats();
}

