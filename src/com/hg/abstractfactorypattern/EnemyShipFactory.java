package com.hg.abstractfactorypattern;

public class EnemyShipFactory implements EnemyFactoryInterface {
    private int power;
    private int damage;
    private String type;

    public EnemyShipFactory(String type, int power, int damage) {
        this.type = type;
        this.power = power;
        this.damage = damage;
    }

    @Override
    public Enemy createEnemy() {
        return new EnemyShip(this.type, this.power, this.damage);
    }
}
