package com.hg.abstractfactorypattern;

public interface EnemyFactoryInterface {
    public Enemy createEnemy();
}
