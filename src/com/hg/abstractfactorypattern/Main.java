package com.hg.abstractfactorypattern;

public class Main {

    public static void main(String[] args) {
        Enemy tank = EnemyFactory.makeEnemy(new EnemyTankFactory("TANK", 70, 55));
        tank.getStats();

        Enemy ship = EnemyFactory.makeEnemy(new EnemyShipFactory("SHIP", 95, 90));
        ship.getStats();
    }
}
