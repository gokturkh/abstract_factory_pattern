package com.hg.abstractfactorypattern;

public class EnemyTank extends Enemy {
    public EnemyTank(String type, int power, int damage) {
        super(type, power, damage);
    }

    @Override
    protected void attack() {
        System.out.println("Tank attacked.");
    }

    @Override
    protected void getStats() {
        System.out.println("Enemy type is: " + this.getType());
        System.out.println("Enemy power is: " + this.getPower());
        System.out.println("Enemy damage is: " + this.getDamage());
    }
}
